describe Fluent::TimeSlicedFilterOutput do
  let(:time) {
    Time.parse('2014-02-08 13:14:15 +0900').to_i
  }

  describe 'when 2 records transmitted per second' do
    it 'should be aggregated to two records' do
      emits = run_driver do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.default\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.default\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.default\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.default\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.default\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.default\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.default\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}]]
      )
    end

    it 'should be rewritten to the specified tag' do
      emits = run_driver(:prefix => 'any_prefix') do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["any_prefix.test.default", 1391832855, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["any_prefix.test.default", 1391832856, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.default\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["any_prefix.test.default", 1391832857, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.default\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["any_prefix.test.default", 1391832858, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.default\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["any_prefix.test.default", 1391832859, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.default\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["any_prefix.test.default", 1391832860, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.default\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["any_prefix.test.default", 1391832861, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.default\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["any_prefix.test.default", 1391832862, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.default\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}]]
      )
    end

    it 'should receive multiple converted records' do
      filter = <<-EOS
        proc {|rs|
          [
            {'foo' => 'bar', 100 => 200},
            {'bar' => 'baz', 200 => 300},
          ]
        }
      EOS

      emits = run_driver(:filter => filter) do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832855, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832856, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832856, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832857, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832857, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832858, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832858, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832859, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832859, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832860, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832860, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832861, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832861, {"bar"=>"baz", 200=>300}],
         ["filtered.test.default", 1391832862, {"foo"=>"bar", 100=>200}],
         ["filtered.test.default", 1391832862, {"bar"=>"baz", 200=>300}]]
      )
    end
  end

  describe 'when 3 records transmitted per second' do
    it 'should be aggregated to two records' do
      emits = run_driver do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
          d.emit({"key#{i}__" => "val#{i}__"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}], [\"test.default\", 1391832855, {\"key0__\"=>\"val0__\"}]]"}],
         ["filtered.test.default", 1391832856, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.default\", 1391832856, {\"key1_\"=>\"val1_\"}], [\"test.default\", 1391832856, {\"key1__\"=>\"val1__\"}]]"}],
         ["filtered.test.default", 1391832857, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.default\", 1391832857, {\"key2_\"=>\"val2_\"}], [\"test.default\", 1391832857, {\"key2__\"=>\"val2__\"}]]"}],
         ["filtered.test.default", 1391832858, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.default\", 1391832858, {\"key3_\"=>\"val3_\"}], [\"test.default\", 1391832858, {\"key3__\"=>\"val3__\"}]]"}],
         ["filtered.test.default", 1391832859, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.default\", 1391832859, {\"key4_\"=>\"val4_\"}], [\"test.default\", 1391832859, {\"key4__\"=>\"val4__\"}]]"}],
         ["filtered.test.default", 1391832860, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.default\", 1391832860, {\"key5_\"=>\"val5_\"}], [\"test.default\", 1391832860, {\"key5__\"=>\"val5__\"}]]"}],
         ["filtered.test.default", 1391832861, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.default\", 1391832861, {\"key6_\"=>\"val6_\"}], [\"test.default\", 1391832861, {\"key6__\"=>\"val6__\"}]]"}],
         ["filtered.test.default", 1391832862, {"count"=>3, "inspect"=>"[[\"test.default\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.default\", 1391832862, {\"key7_\"=>\"val7_\"}], [\"test.default\", 1391832862, {\"key7__\"=>\"val7__\"}]]"}]]
      )
    end
  end

  describe 'when unit time 1m' do
    it 'should be aggregated by every 1 min (aggregated to 2 records)' do
      emits = run_driver(:time_slice_format => '%Y%m%d%H%M') do |d|
        (0...8).each do |i|
          i *= 20
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>6, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}], [\"test.default\", 1391832875, {\"key20\"=>\"val20\"}], [\"test.default\", 1391832875, {\"key20_\"=>\"val20_\"}], [\"test.default\", 1391832895, {\"key40\"=>\"val40\"}], [\"test.default\", 1391832895, {\"key40_\"=>\"val40_\"}]]"}],
         ["filtered.test.default", 1391832915, {"count"=>6, "inspect"=>"[[\"test.default\", 1391832915, {\"key60\"=>\"val60\"}], [\"test.default\", 1391832915, {\"key60_\"=>\"val60_\"}], [\"test.default\", 1391832935, {\"key80\"=>\"val80\"}], [\"test.default\", 1391832935, {\"key80_\"=>\"val80_\"}], [\"test.default\", 1391832955, {\"key100\"=>\"val100\"}], [\"test.default\", 1391832955, {\"key100_\"=>\"val100_\"}]]"}],
         ["filtered.test.default", 1391832975, {"count"=>4, "inspect"=>"[[\"test.default\", 1391832975, {\"key120\"=>\"val120\"}], [\"test.default\", 1391832975, {\"key120_\"=>\"val120_\"}], [\"test.default\", 1391832995, {\"key140\"=>\"val140\"}], [\"test.default\", 1391832995, {\"key140_\"=>\"val140_\"}]]"}]]
      )
    end

    it 'should be aggregated by every 1 min  (aggregated to 3 records)' do
      emits = run_driver(:time_slice_format => '%Y%m%d%H%M') do |d|
        (0...8).each do |i|
          i *= 20
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
          d.emit({"key#{i}__" => "val#{i}__"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>9, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}], [\"test.default\", 1391832855, {\"key0__\"=>\"val0__\"}], [\"test.default\", 1391832875, {\"key20\"=>\"val20\"}], [\"test.default\", 1391832875, {\"key20_\"=>\"val20_\"}], [\"test.default\", 1391832875, {\"key20__\"=>\"val20__\"}], [\"test.default\", 1391832895, {\"key40\"=>\"val40\"}], [\"test.default\", 1391832895, {\"key40_\"=>\"val40_\"}], [\"test.default\", 1391832895, {\"key40__\"=>\"val40__\"}]]"}],
         ["filtered.test.default", 1391832915, {"count"=>9, "inspect"=>"[[\"test.default\", 1391832915, {\"key60\"=>\"val60\"}], [\"test.default\", 1391832915, {\"key60_\"=>\"val60_\"}], [\"test.default\", 1391832915, {\"key60__\"=>\"val60__\"}], [\"test.default\", 1391832935, {\"key80\"=>\"val80\"}], [\"test.default\", 1391832935, {\"key80_\"=>\"val80_\"}], [\"test.default\", 1391832935, {\"key80__\"=>\"val80__\"}], [\"test.default\", 1391832955, {\"key100\"=>\"val100\"}], [\"test.default\", 1391832955, {\"key100_\"=>\"val100_\"}], [\"test.default\", 1391832955, {\"key100__\"=>\"val100__\"}]]"}],
         ["filtered.test.default", 1391832975, {"count"=>6, "inspect"=>"[[\"test.default\", 1391832975, {\"key120\"=>\"val120\"}], [\"test.default\", 1391832975, {\"key120_\"=>\"val120_\"}], [\"test.default\", 1391832975, {\"key120__\"=>\"val120__\"}], [\"test.default\", 1391832995, {\"key140\"=>\"val140\"}], [\"test.default\", 1391832995, {\"key140_\"=>\"val140_\"}], [\"test.default\", 1391832995, {\"key140__\"=>\"val140__\"}]]"}]]
      )
    end
  end

  describe 'when multiple tags' do
    it 'should be output only the first tag' do
      emits = run_driver do |d|
        (0...8).each do |i|
          d.tag = 'test.1'
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.tag = 'test.2'
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.1", 1391832855, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.2\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.1", 1391832856, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.2\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.1", 1391832857, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.2\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.1", 1391832858, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.2\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.1", 1391832859, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.2\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.1", 1391832860, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.2\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.1", 1391832861, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.2\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.1", 1391832862, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.2\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}]]
      )
    end

    it 'should be output to each tag' do
      emits = run_driver(:emit_each_tag => true) do |d|
        (0...8).each do |i|
          d.tag = 'test.1'
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.tag = 'test.2'
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.1", 1391832855, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.2\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.2", 1391832855, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.2\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.1", 1391832856, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.2\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.2", 1391832856, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.2\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.1", 1391832857, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.2\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.2", 1391832857, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.2\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.1", 1391832858, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.2\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.2", 1391832858, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.2\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.1", 1391832859, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.2\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.2", 1391832859, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.2\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.1", 1391832860, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.2\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.2", 1391832860, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.2\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.1", 1391832861, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.2\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.2", 1391832861, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.2\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.1", 1391832862, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.2\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}],
         ["filtered.test.2", 1391832862, {"count"=>2, "inspect"=>"[[\"test.1\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.2\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}]]
      )
    end
  end

  describe 'when the array of Hash is passed' do
    it 'should receive an array of Hash' do
      emits = run_driver(:pass_hash_row => true) do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[{\"key0\"=>\"val0\"}, {\"key0_\"=>\"val0_\"}]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[{\"key1\"=>\"val1\"}, {\"key1_\"=>\"val1_\"}]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[{\"key2\"=>\"val2\"}, {\"key2_\"=>\"val2_\"}]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[{\"key3\"=>\"val3\"}, {\"key3_\"=>\"val3_\"}]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[{\"key4\"=>\"val4\"}, {\"key4_\"=>\"val4_\"}]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[{\"key5\"=>\"val5\"}, {\"key5_\"=>\"val5_\"}]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[{\"key6\"=>\"val6\"}, {\"key6_\"=>\"val6_\"}]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[{\"key7\"=>\"val7\"}, {\"key7_\"=>\"val7_\"}]"}]]
      )
    end

    it 'should receive an array of Hash (include time/tag)' do
      emits = run_driver(:pass_hash_row => true, :include_time_key => true, :include_tag_key => true, :utc => true) do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[{\"key0\"=>\"val0\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:15Z\"}, {\"key0_\"=>\"val0_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:15Z\"}]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[{\"key1\"=>\"val1\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:16Z\"}, {\"key1_\"=>\"val1_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:16Z\"}]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[{\"key2\"=>\"val2\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:17Z\"}, {\"key2_\"=>\"val2_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:17Z\"}]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[{\"key3\"=>\"val3\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:18Z\"}, {\"key3_\"=>\"val3_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:18Z\"}]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[{\"key4\"=>\"val4\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:19Z\"}, {\"key4_\"=>\"val4_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:19Z\"}]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[{\"key5\"=>\"val5\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:20Z\"}, {\"key5_\"=>\"val5_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:20Z\"}]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[{\"key6\"=>\"val6\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:21Z\"}, {\"key6_\"=>\"val6_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:21Z\"}]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[{\"key7\"=>\"val7\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:22Z\"}, {\"key7_\"=>\"val7_\", \"tag\"=>\"test.default\", \"time\"=>\"2014-02-08T04:14:22Z\"}]"}]]
      )
    end

    it 'should receive an array of Hash (include time/tag, not default key)' do
      emits = run_driver(:pass_hash_row => true, :include_time_key => true, :time_key => '__TIME__', :include_tag_key => true, :tag_key => '__TAG__', :utc => true) do |d|
        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[{\"key0\"=>\"val0\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:15Z\"}, {\"key0_\"=>\"val0_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:15Z\"}]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[{\"key1\"=>\"val1\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:16Z\"}, {\"key1_\"=>\"val1_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:16Z\"}]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[{\"key2\"=>\"val2\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:17Z\"}, {\"key2_\"=>\"val2_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:17Z\"}]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[{\"key3\"=>\"val3\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:18Z\"}, {\"key3_\"=>\"val3_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:18Z\"}]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[{\"key4\"=>\"val4\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:19Z\"}, {\"key4_\"=>\"val4_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:19Z\"}]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[{\"key5\"=>\"val5\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:20Z\"}, {\"key5_\"=>\"val5_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:20Z\"}]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[{\"key6\"=>\"val6\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:21Z\"}, {\"key6_\"=>\"val6_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:21Z\"}]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[{\"key7\"=>\"val7\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:22Z\"}, {\"key7_\"=>\"val7_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:22Z\"}]"}],
         ["filtered.test.default", 1391832863, {"count"=>2, "inspect"=>"[{\"key8\"=>\"val8\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:23Z\"}, {\"key8_\"=>\"val8_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:23Z\"}]"}],
         ["filtered.test.default", 1391832864, {"count"=>2, "inspect"=>"[{\"key9\"=>\"val9\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:24Z\"}, {\"key9_\"=>\"val9_\", \"__TAG__\"=>\"test.default\", \"__TIME__\"=>\"2014-02-08T04:14:24Z\"}]"}]]
      )
    end
  end

  describe 'when an invalid record was included' do
    it 'should skip the bad records (Filter returns a Hash)' do
      filter = <<-EOS
        proc {|rs|
          if rs.any? {|i| i[2].has_key?('return_nil')}
            nil
          else
            {
              'count'   => rs.count,
              'inspect' => rs.inspect
            }
          end
        }
      EOS

      emits = run_driver(:filter => filter) do |d|
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: nil (NilClass)') }

        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end

        d.emit({"return_nil" => 1}, time + 10)

        (10...20).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.default\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.default\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.default\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.default\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.default\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.default\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.default\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}],
         ["filtered.test.default", 1391832863, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832863, {\"key8\"=>\"val8\"}], [\"test.default\", 1391832863, {\"key8_\"=>\"val8_\"}]]"}],
         ["filtered.test.default", 1391832864, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832864, {\"key9\"=>\"val9\"}], [\"test.default\", 1391832864, {\"key9_\"=>\"val9_\"}]]"}],
         ["filtered.test.default", 1391832866, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832866, {\"key11\"=>\"val11\"}], [\"test.default\", 1391832866, {\"key11_\"=>\"val11_\"}]]"}],
         ["filtered.test.default", 1391832867, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832867, {\"key12\"=>\"val12\"}], [\"test.default\", 1391832867, {\"key12_\"=>\"val12_\"}]]"}],
         ["filtered.test.default", 1391832868, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832868, {\"key13\"=>\"val13\"}], [\"test.default\", 1391832868, {\"key13_\"=>\"val13_\"}]]"}],
         ["filtered.test.default", 1391832869, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832869, {\"key14\"=>\"val14\"}], [\"test.default\", 1391832869, {\"key14_\"=>\"val14_\"}]]"}],
         ["filtered.test.default", 1391832870, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832870, {\"key15\"=>\"val15\"}], [\"test.default\", 1391832870, {\"key15_\"=>\"val15_\"}]]"}],
         ["filtered.test.default", 1391832871, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832871, {\"key16\"=>\"val16\"}], [\"test.default\", 1391832871, {\"key16_\"=>\"val16_\"}]]"}],
         ["filtered.test.default", 1391832872, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832872, {\"key17\"=>\"val17\"}], [\"test.default\", 1391832872, {\"key17_\"=>\"val17_\"}]]"}],
         ["filtered.test.default", 1391832873, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832873, {\"key18\"=>\"val18\"}], [\"test.default\", 1391832873, {\"key18_\"=>\"val18_\"}]]"}],
         ["filtered.test.default", 1391832874, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832874, {\"key19\"=>\"val19\"}], [\"test.default\", 1391832874, {\"key19_\"=>\"val19_\"}]]"}]]
      )
    end

    it 'should skip the bad records (Filter returns an Array)' do
      filter = <<-EOS
        proc {|rs|
          if rs.any? {|i| i[2].has_key?('return_nil')}
            [nil, 1]
          else
            {
              'count'   => rs.count,
              'inspect' => rs.inspect
            }
          end
        }
      EOS

      emits = run_driver(:filter => filter) do |d|
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: nil (NilClass)') }
        d.instance.log.should_receive(:warn) {|msg| expect(msg).to eq('Record must be Hash: 1 (Fixnum)') }

        (0...10).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end

        d.emit({"return_nil" => 1}, time + 10)

        (10...20).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832855, {\"key0\"=>\"val0\"}], [\"test.default\", 1391832855, {\"key0_\"=>\"val0_\"}]]"}],
         ["filtered.test.default", 1391832856, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832856, {\"key1\"=>\"val1\"}], [\"test.default\", 1391832856, {\"key1_\"=>\"val1_\"}]]"}],
         ["filtered.test.default", 1391832857, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832857, {\"key2\"=>\"val2\"}], [\"test.default\", 1391832857, {\"key2_\"=>\"val2_\"}]]"}],
         ["filtered.test.default", 1391832858, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832858, {\"key3\"=>\"val3\"}], [\"test.default\", 1391832858, {\"key3_\"=>\"val3_\"}]]"}],
         ["filtered.test.default", 1391832859, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832859, {\"key4\"=>\"val4\"}], [\"test.default\", 1391832859, {\"key4_\"=>\"val4_\"}]]"}],
         ["filtered.test.default", 1391832860, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832860, {\"key5\"=>\"val5\"}], [\"test.default\", 1391832860, {\"key5_\"=>\"val5_\"}]]"}],
         ["filtered.test.default", 1391832861, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832861, {\"key6\"=>\"val6\"}], [\"test.default\", 1391832861, {\"key6_\"=>\"val6_\"}]]"}],
         ["filtered.test.default", 1391832862, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832862, {\"key7\"=>\"val7\"}], [\"test.default\", 1391832862, {\"key7_\"=>\"val7_\"}]]"}],
         ["filtered.test.default", 1391832863, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832863, {\"key8\"=>\"val8\"}], [\"test.default\", 1391832863, {\"key8_\"=>\"val8_\"}]]"}],
         ["filtered.test.default", 1391832864, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832864, {\"key9\"=>\"val9\"}], [\"test.default\", 1391832864, {\"key9_\"=>\"val9_\"}]]"}],
         ["filtered.test.default", 1391832866, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832866, {\"key11\"=>\"val11\"}], [\"test.default\", 1391832866, {\"key11_\"=>\"val11_\"}]]"}],
         ["filtered.test.default", 1391832867, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832867, {\"key12\"=>\"val12\"}], [\"test.default\", 1391832867, {\"key12_\"=>\"val12_\"}]]"}],
         ["filtered.test.default", 1391832868, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832868, {\"key13\"=>\"val13\"}], [\"test.default\", 1391832868, {\"key13_\"=>\"val13_\"}]]"}],
         ["filtered.test.default", 1391832869, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832869, {\"key14\"=>\"val14\"}], [\"test.default\", 1391832869, {\"key14_\"=>\"val14_\"}]]"}],
         ["filtered.test.default", 1391832870, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832870, {\"key15\"=>\"val15\"}], [\"test.default\", 1391832870, {\"key15_\"=>\"val15_\"}]]"}],
         ["filtered.test.default", 1391832871, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832871, {\"key16\"=>\"val16\"}], [\"test.default\", 1391832871, {\"key16_\"=>\"val16_\"}]]"}],
         ["filtered.test.default", 1391832872, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832872, {\"key17\"=>\"val17\"}], [\"test.default\", 1391832872, {\"key17_\"=>\"val17_\"}]]"}],
         ["filtered.test.default", 1391832873, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832873, {\"key18\"=>\"val18\"}], [\"test.default\", 1391832873, {\"key18_\"=>\"val18_\"}]]"}],
         ["filtered.test.default", 1391832874, {"count"=>2, "inspect"=>"[[\"test.default\", 1391832874, {\"key19\"=>\"val19\"}], [\"test.default\", 1391832874, {\"key19_\"=>\"val19_\"}]]"}]]
      )
    end
  end

  describe 'when the non-Proc filter passed' do
    it 'should be filtered' do
      filter = <<-EOS
        class Filter
          def call(records)
            @total_count ||= 0
            @total_count += records.count
            {'total_count' => @total_count}
          end
        end
        Filter.new
      EOS

      emits =run_driver(:filter => filter) do |d|
        (0...8).each do |i|
          d.emit({"key#{i}" => "val#{i}"}, time + i)
          d.emit({"key#{i}_" => "val#{i}_"}, time + i)
        end
      end

      expect(emits).to eq(
        [["filtered.test.default", 1391832855, {"total_count"=>2}],
         ["filtered.test.default", 1391832856, {"total_count"=>4}],
         ["filtered.test.default", 1391832857, {"total_count"=>6}],
         ["filtered.test.default", 1391832858, {"total_count"=>8}],
         ["filtered.test.default", 1391832859, {"total_count"=>10}],
         ["filtered.test.default", 1391832860, {"total_count"=>12}],
         ["filtered.test.default", 1391832861, {"total_count"=>14}],
         ["filtered.test.default", 1391832862, {"total_count"=>16}]]
      )
    end
  end
end
