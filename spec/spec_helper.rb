require 'fluent/test'
require 'fluent/plugin/out_time_sliced_filter'
require 'tempfile'
require 'tmpdir'
require 'time'

# Disable Test::Unit
module Test::Unit::RunCount; def run(*); end; end

RSpec.configure do |config|
  config.before(:all) do
    Fluent::Test.setup
  end
end

def tempfile(content, basename = nil)
  basename ||= "#{File.basename __FILE__}.#{$$}"

  Tempfile.open(basename) do |f|
    f << content
    f.flush
    f.rewind
    yield(f)
  end
end

def run_driver(options = {})
  options = options.dup

  filter = options[:filter] || (<<-EOS)
proc {|rs|
  {
    'count'   => rs.count,
    'inspect' => rs.inspect
  }
}
  EOS

  tag = options[:tag] || 'test.default'
  time_slice_format = options[:time_slice_format] || '%Y%m%d%H%M%S'

  option_keys = [
    :prefix,
    :emit_each_tag,
    :pass_hash_row,
    :include_time_key,
    :include_tag_key,
    :time_key,
    :tag_key,
    :utc,
  ]

  additional_options = option_keys.map {|key|
    if options[key]
      "#{key} #{options[key]}"
    end
  }.join("\n")

  Dir.mktmpdir do |tmpdir|
    tempfile(filter, options[:tempfile]) do |f|
      fluentd_conf = <<-EOS
filter_path #{f.path}
buffer_path #{tmpdir}/*
time_slice_format #{time_slice_format}
#{additional_options}
      EOS

      driver = Fluent::Test::OutputTestDriver.new(Fluent::TimeSlicedFilterOutput, tag).configure(fluentd_conf)

      driver.run do
        yield(driver)

        driver.instance.enqueue_buffer
        buffer = driver.instance.instance_variable_get(:@buffer)

        # XXX: The last one is not added to the queue...
        until buffer.queue_size.zero?
          driver.instance.try_flush
        end
      end

      driver.emits
    end
  end
end
