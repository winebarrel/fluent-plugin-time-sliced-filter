# fluent-plugin-time-sliced-filter

TimeSlicedOutput Plugin to aggregate by unit time

[![Gem Version](https://badge.fury.io/rb/fluent-plugin-time-sliced-filter.png)](http://badge.fury.io/rb/fluent-plugin-time-sliced-filter)
[![Build Status](https://drone.io/bitbucket.org/winebarrel/fluent-plugin-time-sliced-filter/status.png)](https://drone.io/bitbucket.org/winebarrel/fluent-plugin-time-sliced-filter/latest)

## Installation

    $ gem install fluent-plugin-time-sliced-filter

## Configuration

```
<match my.**>
  type time_sliced_filter
  filter_path /foo/bar/my_filrer.rb
  time_slice_format %Y%m%d%H%M%S
  buffer_path /any_path
  #prefix filtered
  #emit_each_tag false
  #pass_hash_row false
  #include_tag_key false
  #include_time_key false
</match>

<match filtered.my.**>
  type stdout
</match>
```

## Usage

```ruby
# Count the number of records of every unit time

proc {|records|

  # `records` is an Array such as:
  #   [[tag, time, record], [tag, time, record], ...]
  #
  # e.g.)
  #   [["my.data", 1391820170, {"hoge"=>"fuga"}],
  #    ["my.data", 1391820170, {"hoge"=>"fuga"}],
  #    ["my.data", 1391820170, {"hoge"=>"fuga"}],
  #    ...

  {'count' => records.count}
  # or [{...},{...},{...}, ...]
}
```

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
